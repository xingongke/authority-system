package com.chaoxing.user.controller;


import com.chaoxing.user.beans.DTO.Result;
import com.chaoxing.user.service.IAreaService;
import com.chaoxing.user.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Api(tags = "地区模块")
public class AreaController {

    @Autowired
    private IAreaService areaService ;

    @GetMapping("/selectAreaListById")
    @ApiOperation("级联查询地区")
    @ResponseBody
    public Result selectAreaListById(String id) {
        return areaService.selectAreaListById(id);
    }


    @GetMapping("/selectAreaDataById")
    @ApiOperation("级联查询地区树数据结构")
    @ResponseBody
//   参数 id  对应的 是 parentId
    public Result selectAreaDataById( String id) {
        return areaService.selectAreaDataById(id);
    }





}
