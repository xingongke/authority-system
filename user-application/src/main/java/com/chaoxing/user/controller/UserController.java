package com.chaoxing.user.controller;


import com.chaoxing.user.beans.DTO.Result;
import com.chaoxing.user.beans.VO.RegisterVO;
import com.chaoxing.user.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Api(tags = "后台接口模块")
public class UserController {

    @Autowired
    private IUserService userService ;

    @PostMapping("/register")
    @ApiOperation("注册")
    @ResponseBody
    public Result register(RegisterVO registerVO) {

        return userService.register(registerVO);
    }


}
