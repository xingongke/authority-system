package com.chaoxing.user.service;

import com.chaoxing.user.beans.DTO.Result;

public interface IAreaService {
    Result selectAreaListById(String id);

    Result selectAreaDataById(String id);
}
