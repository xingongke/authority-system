package com.chaoxing.user.dao.mapper;

import com.chaoxing.user.beans.DO.BookAuth;
import tk.mybatis.mapper.common.Mapper;

public interface BookAuthMapper extends Mapper<BookAuth> {
}