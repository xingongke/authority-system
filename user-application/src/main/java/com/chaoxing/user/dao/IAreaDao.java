package com.chaoxing.user.dao;

import com.chaoxing.user.beans.DO.AreaInfo;

import java.util.List;

public interface IAreaDao {
    List<AreaInfo> selectAreaListById(String id);

    AreaInfo selectAreaInfoById(String id);
}
