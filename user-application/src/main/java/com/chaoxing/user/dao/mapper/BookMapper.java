package com.chaoxing.user.dao.mapper;

import com.chaoxing.user.beans.DO.Book;
import tk.mybatis.mapper.common.Mapper;

public interface BookMapper extends Mapper<Book> {
}