package com.chaoxing.user.dao.impl;

import com.chaoxing.user.beans.DO.UserInfo;
import com.chaoxing.user.dao.IUserDao;
import com.chaoxing.user.dao.mapper.UserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author dingyuanyuan
 */
@Repository
public class UserDaoImpl implements IUserDao {

    @Autowired
    private UserInfoMapper userInfoMapper ;

    @Override
    public int inserUserInfo(UserInfo userInfo) {
        return userInfoMapper.insert(userInfo) ;
    }
}
