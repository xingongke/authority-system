package com.chaoxing.user.dao.mapper;

import com.chaoxing.user.beans.DO.AreaInfo;
import tk.mybatis.mapper.common.Mapper;

public interface AreaInfoMapper extends Mapper<AreaInfo> {
}