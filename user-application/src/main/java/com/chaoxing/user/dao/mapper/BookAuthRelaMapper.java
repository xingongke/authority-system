package com.chaoxing.user.dao.mapper;

import com.chaoxing.user.beans.DO.BookAuthRela;
import tk.mybatis.mapper.common.Mapper;

public interface BookAuthRelaMapper extends Mapper<BookAuthRela> {
}