package com.chaoxing.user.dao.impl;

import com.chaoxing.user.beans.DO.AreaInfo;
import com.chaoxing.user.dao.IAreaDao;
import com.chaoxing.user.dao.mapper.AreaInfoMapper;
import com.chaoxing.user.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @author dingyuanyuan
 */
@Repository
public class AreaDaoImpl implements IAreaDao {

    @Autowired
    private AreaInfoMapper areaInfoMapper ;

    @Override
    public List<AreaInfo> selectAreaListById(String parentId) {
        Example example = new Example(AreaInfo.class);
        example.createCriteria()
                .andEqualTo("parentId",parentId)
                .andEqualTo("status", Constant.STATUS_ENABLE);
        return areaInfoMapper.selectByExample(example);
    }

    @Override
    public AreaInfo selectAreaInfoById(String id) {
        Example example = new Example(AreaInfo.class);
        example.createCriteria()
                .andEqualTo("id",id)
                .andEqualTo("status", Constant.STATUS_ENABLE);
        return areaInfoMapper.selectOneByExample(example);
    }
}
