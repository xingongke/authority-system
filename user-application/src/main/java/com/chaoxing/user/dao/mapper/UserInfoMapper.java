package com.chaoxing.user.dao.mapper;

import com.chaoxing.user.beans.DO.UserInfo;
import tk.mybatis.mapper.common.Mapper;

public interface UserInfoMapper extends Mapper<UserInfo> {
}