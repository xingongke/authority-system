package com.chaoxing.user.convert;

import com.chaoxing.user.beans.DO.UserInfo;
import com.chaoxing.user.beans.VO.RegisterVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserInfoConvert {

    UserInfoConvert INSTANCE = Mappers.getMapper(UserInfoConvert.class);

    @Mappings({
            @Mapping(source = "name", target = "userName"),
            @Mapping(source = "account", target = "userAccount"),
            @Mapping(source = "password", target = "userPassword"),
    })
    UserInfo userVotoUserInfo(RegisterVO registerVO);
}
