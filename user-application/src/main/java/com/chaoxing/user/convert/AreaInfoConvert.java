package com.chaoxing.user.convert;

import com.chaoxing.user.beans.DO.AreaInfo;
import com.chaoxing.user.beans.DO.UserInfo;
import com.chaoxing.user.beans.DTO.AreaInfoDTO;
import com.chaoxing.user.beans.VO.RegisterVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AreaInfoConvert {
    AreaInfoConvert INSTANCE = Mappers.getMapper(AreaInfoConvert.class);

    AreaInfoDTO do2Dto(AreaInfo areaInfo) ;

    List<AreaInfoDTO> doList2DtoList(List<AreaInfo> areaInfos);
}
