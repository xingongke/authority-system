package com.chaoxing.user.beans.DO;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author dingyuanyuan
 */
@Table(name = "book_auth")
public class BookAuth {
    /**
     * id
     */
    @Id
    private Long id;

    /**
     * 作者名称
     */
    @Column(name = "auth_name")
    private String authName;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 地址
     */
    private String addr;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 状态：0正常 1删除
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 获取id
     *
     * @return id - id
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置id
     *
     * @param id id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取作者名称
     *
     * @return auth_name - 作者名称
     */
    public String getAuthName() {
        return authName;
    }

    /**
     * 设置作者名称
     *
     * @param authName 作者名称
     */
    public void setAuthName(String authName) {
        this.authName = authName;
    }

    /**
     * 获取年龄
     *
     * @return age - 年龄
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 设置年龄
     *
     * @param age 年龄
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 获取地址
     *
     * @return addr - 地址
     */
    public String getAddr() {
        return addr;
    }

    /**
     * 设置地址
     *
     * @param addr 地址
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取状态：0正常 1删除
     *
     * @return status - 状态：0正常 1删除
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置状态：0正常 1删除
     *
     * @param status 状态：0正常 1删除
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}