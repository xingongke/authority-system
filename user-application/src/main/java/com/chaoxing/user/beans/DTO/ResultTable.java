package com.chaoxing.user.beans.DTO;


import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel("响应对象")
@Data
public class ResultTable<T> {


    private Integer code;
    private long count;
    private T data;
    private String massage;
}
