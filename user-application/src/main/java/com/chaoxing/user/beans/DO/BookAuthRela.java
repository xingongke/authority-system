package com.chaoxing.user.beans.DO;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "book_auth_rela")
public class BookAuthRela {
    @Column(name = "book_id")
    private Long bookId;

    @Column(name = "auth_id")
    private Long authId;

    /**
     * @return book_id
     */
    public Long getBookId() {
        return bookId;
    }

    /**
     * @param bookId
     */
    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    /**
     * @return auth_id
     */
    public Long getAuthId() {
        return authId;
    }

    /**
     * @param authId
     */
    public void setAuthId(Long authId) {
        this.authId = authId;
    }
}