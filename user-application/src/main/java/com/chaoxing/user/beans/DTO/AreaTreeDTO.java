package com.chaoxing.user.beans.DTO;

import lombok.Data;

import java.util.List;

@Data
public class AreaTreeDTO {

//    当前地区
    private AreaInfoDTO areaInfoDTO ;

//    当前地区的下一级地区
    private List<AreaTreeDTO> areaTreeDTOS  ;

}
